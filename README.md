# Bioinformatics
Bioinformatics course project for year 2019./2020.<br>
Course is part for the teaching program of Faculty of Electrical Engineering and Computing, University of Zagreb.<br>
Course link: [https://www.fer.unizg.hr/en/course/bio](https://www.fer.unizg.hr/en/course/bio)<br>
Implementation and documentation is presented by enrolled students.<br>


# Overview
Project implements and describes the pattern matching algorithm of Gonzalo Navarro.<br>
Navarro algorithm link: [https://www.sciencedirect.com/science/article/pii/S0304397599003333](https://www.sciencedirect.com/science/article/pii/S0304397599003333)<br>

Implementation validation and performance testing is done by comparing with the Bit parallel sequence-to-graph alignment algorithm.<br>
Bit parallel algorithm link: [https://academic.oup.com/bioinformatics/article/35/19/3599/5372677](https://academic.oup.com/bioinformatics/article/35/19/3599/5372677)<br>

# Compilation
The project consists of only one C file.<br>
Standard GNU Compiler Collectin (gcc) compilation shown in the next line.<br>
gcc -o navarro.out src/navaroo.c<br>

# Usage
The compiled file is runned with six flags: g, f, o, c, k and d.<br>
Flag information:<br>
    -g      defines the path to a file which contains the sequence graph in GFA format<br>
    -f      defines the path to a file which contains the pattern in FASTQ format<br>
    -o      defines the path to a file which will contant our edit distance results<br>
    -c      defines the path to a file with edit distance results for the Bit parallel algorithm<br>
    -k      defines the max edit distance<br>
    -d      defines the path to a file with detailed outputs<br>
<br>
Code execution example:<br>
./navarro.out -g ./GraphAligner-PaperExperiments/WabiExperimentSnake/tmp/ref10000_onechar.gfa  -f ./GraphAligner-PaperExperiments/WabiExperimentSnake/tmp/ref10000_simulatedreads.fastq -o result.txt -c ./GraphAligner-PaperExperiments/WabiExperimentSnake/queries_results/ref10000_onechar.txt -k 100 -d ./detailed_results.txt
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <limits>
#include "Aligner.h"
#include "ThreadReadAssertion.h"
#include "ByteStuff.h"

int main(int argc, char** argv)
{
#ifndef NOBUILTINPOPCOUNT
	if (__builtin_cpu_supports("popcnt") == 0)
	{
		std::cerr << "CPU does not support builtin popcount operation" << std::endl;
		std::cerr << "recompile with -DNOBUILTINPOPCOUNT" << std::endl;
		std::abort();
	}
#endif

    AlignerParams params;
	params.graphFile = "";
	params.fastqFile = "";
	params.seedFile = "";
	params.numThreads = 0;
	params.initialBandwidth = 0;
	params.rampBandwidth = 0;
	params.dynamicRowStart = 64;
	params.maxCellsPerSlice = std::numeric_limits<decltype(params.maxCellsPerSlice)>::max();
	bool initialFullBand = false;
	params.quietMode = false;
	params.sloppyOptimizations = false;
	params.linear = false;
	int c;

	std::string resultFileName;

	while ((c = getopt(argc, argv, "r:g:f:l")) != -1)
	{
		switch(c)
		{
			case 'g':
				params.graphFile = std::string(optarg);
				break;
			case 'r':
				resultFileName = std::string(optarg);
				break;
			case 'f':
				params.fastqFile = std::string(optarg);
				break;
			case 'l':
				params.linear = true;
				break;
		}
	}

	if (params.graphFile == "")
	{
		std::cerr << "graph file must be set" << std::endl;
		std::exit(0);
	}
	if (params.fastqFile == "")
	{
		std::cerr << "read file must be set" << std::endl;
		std::exit(0);
	}

	// ByteStuff::precalculateByteStuff();

	wabiExperiments(params,resultFileName);
	// alignReads(params);

	return 0;
}

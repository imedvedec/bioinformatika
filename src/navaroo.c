#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>

#define INT_MAX +2147483647
#define MODE_COUNT_NODES 0
#define MODE_COUNT_EDGES 1
#define IS_PART_OF_COMPLEMENT_STRAND 1
#define IS_NOT_PART_OF_COMPLEMENT_STRAND 0

//List of nodes (item in list)
struct IntNode {
    int uid;
    struct IntNode *next;
};

//List of nodes (head)
struct NeighbourList {
    struct IntNode *head;
};

//Node structure
struct Node {
    long uid;
    char text;
    int c, c_dash;
    struct NeighbourList *inNeighbourList;
    struct NeighbourList *outNeighbourList;


    //data describing node from original input graph that contained character represented by this node
    long originalNodeId;
    long indexInOriginalNode;

    int isPartOfComplementStrand;
};

//Edge structure (list)
struct Edge {
    int source;
    int destination;
    struct Edge *next;
};

//Graph structure
struct Graph {
    int size;
    struct Node **nodes;
    struct Edge *edgesHead;
};

//Construct a new node
struct Node *NodeConstructor(int uid, char text, long originalNodeId, int indexInOriginalNode, int isPartOfComplementStrand) {
    struct Node *newNode = (struct Node *) malloc(sizeof(struct Node));
    newNode->uid = uid;
    newNode->text = text;
    newNode->c = 0;
    newNode->c_dash = 0;
    newNode->inNeighbourList = (struct NeighbourList *) malloc(sizeof(struct NeighbourList));
    newNode->inNeighbourList->head = NULL;
    newNode->outNeighbourList = (struct NeighbourList *) malloc(sizeof(struct NeighbourList));
    newNode->outNeighbourList->head = NULL;
    newNode->originalNodeId = originalNodeId;
    newNode->indexInOriginalNode = indexInOriginalNode;
    newNode->isPartOfComplementStrand = isPartOfComplementStrand;

    return newNode;
}

//Graph construction from a text string. DEPRECATED. Used for inital testing.
struct Graph *createGraph(char *text, int size) {
    struct Graph *graph = (struct Graph *) malloc(sizeof(struct Graph));
    graph->size = size;

    graph->nodes = (struct Node **) malloc(size * sizeof(struct Node));
    for (int i = 0; i < size; i++) {
        graph->nodes[i] = NodeConstructor(i, *(text + i),i,0,0);
    }

    graph->edgesHead = NULL;

    return graph;
}

//Edge addition to a defined graph
void addEdge(struct Graph *graph, int source, int destination) {
    struct IntNode *listHeadIn = graph->nodes[destination]->inNeighbourList->head;
    struct IntNode *newIntNodeIn = (struct IntNode *) malloc(sizeof(struct IntNode));
    newIntNodeIn->uid = source;
    newIntNodeIn->next = listHeadIn;
    graph->nodes[destination]->inNeighbourList->head = newIntNodeIn;

    struct IntNode *listHeadOut = graph->nodes[source]->outNeighbourList->head;
    struct IntNode *newIntNodeOut = (struct IntNode *) malloc(sizeof(struct IntNode));
    newIntNodeOut->uid = destination;
    newIntNodeOut->next = listHeadOut;
    graph->nodes[source]->outNeighbourList->head = newIntNodeOut;

    struct Edge *newEdge = (struct Edge *) malloc(sizeof(struct Edge));
    newEdge->source = source;
    newEdge->destination = destination;
    newEdge->next = graph->edgesHead;
    graph->edgesHead = newEdge;
}

//Test print function
void printGraph(struct Graph *graph) {
    for (int i = 0; i < graph->size; i++) {
        //printf("%s %ld(%c)\n", "Incoming nodes for node", graph->nodes[i]->uid, graph->nodes[i]->text);
        struct IntNode *neighbourNode = graph->nodes[i]->inNeighbourList->head;
        while (neighbourNode) {
           // printf("%ld(%c) ", graph->nodes[neighbourNode->uid]->uid, graph->nodes[neighbourNode->uid]->text);
            neighbourNode = neighbourNode->next;
        }
        //printf("\n");
    }
}

//Function "minValueEdge" return structure
struct Values {
    int c;
};

//Get minimum C value from neighbour nodes that go into parameter "node"
struct Values minValueEdge(struct Graph *graph, struct Node *node) {
    struct IntNode *head = node->inNeighbourList->head;
    int minValueC = INT_MAX;
    //int minValueC_dash = INT_MAX;
    while (head) {
        int nodeValueC = graph->nodes[head->uid]->c;
        if (nodeValueC < minValueC) {
            minValueC = nodeValueC;
        }

        head = head->next;
    }

    struct Values values = {minValueC};
    return values;
}

//G function from algorithm
int calculationFunction(struct Graph *graph, struct Node *node, char patternText, int patternIndex) {
    int result = patternIndex; // Paper uses 1 based array index, we use 0 based so no -1

    if (node->inNeighbourList->head != NULL) {
        struct Values values = minValueEdge(graph, node);
        if (node->text == patternText) {
            result = values.c < result ? values.c : result;
        } else {
            int min = values.c < node->c ? values.c : node->c;
            result = 1 + min;
        }
    } else if (node->text != patternText) {
        result = 1 + node->c;
    }

    return result;
}


//Propagate function from algorithm
void propagate(struct Graph *graph, int source, int destination) {
    if (graph->nodes[destination]->c > (1 + graph->nodes[source]->c)) {
        graph->nodes[destination]->c = 1 + graph->nodes[source]->c;
        struct IntNode *iterator = graph->nodes[destination]->outNeighbourList->head;
        while (iterator) {
            propagate(graph, destination, iterator->uid);
            iterator = iterator->next;
        }
    }
}

void navarooAlgorithm(struct Graph *graph, char *pattern) {
    int patternSize = strlen(pattern);
    for (int i = 0; i < patternSize; i++) {
        //Calculate C_dash for all nodes
        for (int j = 0; j < graph->size; j++) {
            struct Node *node = graph->nodes[j];
            node->c_dash = calculationFunction(graph, node, pattern[i], i);
        }
        //Update C values from C_dash for all nodes
        for (int j = 0; j < graph->size; j++) {
            graph->nodes[j]->c = graph->nodes[j]->c_dash;
        }
        //Calculate propagate
        struct Edge *edge = graph->edgesHead;
        while (edge) {
            propagate(graph, edge->source, edge->destination);
            edge = edge->next;
        }
    }
}


int countNodesOrEdges(const char *filename, int mode,int *originalNodesCount) {
    int counter = 0;
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    char *text;

    fp = fopen(filename, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    if(originalNodesCount!=NULL) {
      *originalNodesCount = 0;
    }

    while ((getline(&line, &len, fp)) != -1) {
        if (!line) continue;
        
        if(line[0] == 'S' || line[0] == 'L') {
          strtok(line, " \t");
          strtok(NULL, " \t");
      text = strtok(NULL, "\t");
      text = strtok(text,"\n");
        } else {
          continue;
        }
        if (line[0] == 'S' && mode==MODE_COUNT_NODES) {
          counter += strlen(text);
          *originalNodesCount = *originalNodesCount + 1;
        }
        if (line[0] == 'L' && mode==MODE_COUNT_EDGES) {
          counter++;
        }
        if (line[0] == 'S' && mode==MODE_COUNT_EDGES) {
          counter += strlen(text)-1; //counting added edges between characters in multicharacter node
        }
    }

    return counter;
}

int countNodes(const char *filename, int *originalNodesCount) {
    return countNodesOrEdges(filename, MODE_COUNT_NODES,originalNodesCount);
}

int countEdges(const char *filename) {
    return countNodesOrEdges(filename, MODE_COUNT_EDGES,NULL);
}

char getComplement(const char letter){
    switch (letter) {
        case 'A':
        case 'a':
            return 'T';
        case 'C':
        case 'c':
            return 'G';
        case 'T':
        case 't':
            return 'A';
        case 'G':
        case 'g':
            return 'C';
        default:
            exit(-1);
    }
}

struct Graph *loadGraphFromFile(const char *filename) {
    FILE *fp;
    size_t bufferSize = 1000000;
    char *line = (char *)malloc(bufferSize);
    ssize_t read;

    fp = fopen(filename, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    int originalNodesCount;
    int nodeCount = countNodes(filename,&originalNodesCount) * 2;
    int edgeCount = countEdges(filename);
    //printf("nc %d onc %d",nodeCount,originalNodesCount);

    struct Graph *graph = (struct Graph *) malloc(sizeof(struct Graph));
    graph->size = nodeCount;

    graph->nodes = (struct Node **) malloc(nodeCount * sizeof(struct Node));


    int *originalIdToFirstUidMap = (int *) malloc(sizeof(int)*(originalNodesCount+1)); //maps id of original node containing multiple characters to uid of node containing the first character
    int *originalIdToLastUidMap = (int *) malloc(sizeof(int)*(originalNodesCount+1)); //maps id of original node containing multiple characters to uid of node containing the last character
    int *originalIdToFirstComplementStrandUidMap = (int *) malloc(sizeof(int)*(originalNodesCount+1)); //same as originalIdToFirstUidMap but for complement strand part of graph
    int *originalIdToLastComplementStrandUidMap = (int *) malloc(sizeof(int)*(originalNodesCount+1)); //same as originalIdToLastUidMap but for complement strand part of graph

    int nodeIdCounter = 0;

    while (fgets(line,bufferSize,fp)!= NULL) {//((read = getline(&line, &bufferSize, fp)) != -1) {
        //printf("Retrieved line of length %zu:\n", read);
       // printf("%s", line);
        char *type = strtok(line, " \t");
        if (!line) continue;
        if (type[0] != 'S') continue;

        
        char *end;
        char *id_str = strtok(NULL, " \t");
        int id = (int)strtol(id_str, &end, 10);
        char *text = strtok(NULL, "\t");
        text = strtok(text,"\n");
        int textLen = strlen(text);

        for(int i = 0; i < textLen; i++) {
          graph->nodes[nodeIdCounter] = NodeConstructor(nodeIdCounter, text[i], id, i,IS_NOT_PART_OF_COMPLEMENT_STRAND);
          if(i > 0) {
            addEdge(graph, nodeIdCounter-1, nodeIdCounter);
          } 
          if(i==0) {
              originalIdToFirstUidMap[id] = nodeIdCounter;
          }
          if(i==textLen-1) {
            originalIdToLastUidMap[id] = nodeIdCounter;
          }
          nodeIdCounter++;
        }

        for(int i = textLen-1; i >= 0; i--) {
          graph->nodes[nodeIdCounter] = NodeConstructor(nodeIdCounter, getComplement(text[i]), id, i,IS_PART_OF_COMPLEMENT_STRAND);
          if(i < textLen-1) {
            addEdge(graph, nodeIdCounter-1, nodeIdCounter);
          }
          if(i==0) {
              originalIdToLastComplementStrandUidMap[id] = nodeIdCounter;
          }
          if(i==textLen-1) {
            originalIdToFirstComplementStrandUidMap[id] = nodeIdCounter;
          }
          nodeIdCounter++;
        }
       
    }

    rewind(fp);
    while (fgets(line,bufferSize,fp)!= NULL) {
        //printf("%s", line);
        char *type = strtok(line, " \t");
        if (!line) continue;
        if (type[0] != 'L') continue;

        char *end;
        int source = (int)strtol(strtok(NULL, " \t"), &end, 10);
        char *sourceType = strtok(NULL, " \t");
        int destination = (int)strtol(strtok(NULL, " \t"), &end, 10);
        char *destinationType = strtok(NULL, " \t");

        int sourceNodeId = originalIdToLastUidMap[source];
        int destNodeId = originalIdToFirstUidMap[destination];
        addEdge(graph, sourceNodeId, destNodeId);

        int complStrandDestNodeId = originalIdToFirstComplementStrandUidMap[source];
        int complStrandSourceNodeId = originalIdToLastComplementStrandUidMap[destination];
        addEdge(graph, complStrandSourceNodeId, complStrandDestNodeId);
    }

    printGraph(graph);
    fclose(fp);
    free(line);

    return graph;
}

typedef struct SeqNode {
    char *sequence;
    struct SeqNode *next;
} SequenceNode;

typedef struct {
    SequenceNode *head;
    SequenceNode *tail;
    int size;
} SequenceList;

SequenceList *SequenceListConstructor() {
    SequenceList *list = (SequenceList *) malloc(sizeof(SequenceList));
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    return list;
}

void addToSequenceList(SequenceList *list, char *sequence) {
    SequenceNode *node = (SequenceNode *) malloc(sizeof(SequenceNode));
    node->sequence = sequence;
    node->next = NULL;
    if (list->head == NULL) {
        list->head = node;
        list->tail = node;
    }
    list->tail->next = node;
    list->tail = node;
    list->size++;
}

SequenceList *loadSequencesFromFastq(char *fastgFile, int *sequencesNum) {
    size_t bufferSize = 1000;
    char *buffer = (char *) malloc(bufferSize);

    FILE *file = fopen(fastgFile, "r");
    SequenceList *sequences = SequenceListConstructor();

    int charsRead;
    while ((charsRead = getline(&buffer, &bufferSize, file)) != -1) {
        if (buffer[0] != '@') {
            continue;
        }
        charsRead = getline(&buffer, &bufferSize, file);
        if (charsRead == -1) {
            printf("Fastq file parsing error. Exiting program.");
            exit(1);
        }
        char *sequence = strtok(buffer, "\n"); //removes the trailing \n
        char *sequenceCopy = malloc(strlen(sequence) + 1);
        strcpy(sequenceCopy, sequence);
        addToSequenceList(sequences, sequenceCopy);
    }
    fclose(file);

    return sequences;
}

struct Score {
    int editDistance;
    long originalNodeId;
    long indexInOriginalNode;
    int isPartOfComplementStrand;
    struct Score *nextScore;
};

struct Score* getScoresFromGraphAndResetValues(struct Graph *graph, int k, int *minEditDistance) {
    int minC;
    struct Score *score = NULL;
    for (int i = 0; i < graph->size; i++) {
        struct Node *node = graph->nodes[i];
        if (i == 0 || node->c < minC) {
            minC = node->c;
        }
        if(node->c <= k) {
            struct Score *newScore = malloc(sizeof(struct Score));
            newScore->editDistance = node->c;
            newScore->originalNodeId = node->originalNodeId;
            newScore->indexInOriginalNode = node->indexInOriginalNode;
            newScore->isPartOfComplementStrand = node->isPartOfComplementStrand;
            newScore->nextScore = score;
            score = newScore;
        }
        node->c = 0;
        node->c_dash = 0;
    }
    *minEditDistance = minC;
    return score;
}

void printDetailedScores(struct Score **detailedScores, int n, char *detailedOutputFile) {
    FILE *file = fopen(detailedOutputFile, "w");
    for (int i = 0; i < n; i++) {
        fprintf(file, "Scores for pattern %d:\n", i+1);
        fprintf(file, "        Node id\t  Index in node\t  Edit distance\tIs part of complement strand\n");
        struct Score *score = detailedScores[i];
        while(score!=NULL) {
            fprintf(file, "%15ld\t%15ld\t%15d\t%15d\n", score->originalNodeId,score->indexInOriginalNode,score->editDistance,score->isPartOfComplementStrand);
            score = score->nextScore;
        }
        fprintf(file, "\n\n--------------------------------------------------------------------------------\n\n\n");
    }

    fclose(file);
}

void printResults(int *results, int n, char *outputFileName) {
    FILE *file = fopen(outputFileName, "w");
    for (int i = 0; i < n; i++) {
        fprintf(file, "%d\n", results[i]);
    }

    fclose(file);
}

void compareResultsToResultsInCompareFile(int *scores, int n, char *compareFileName) {
    printf("COMPARING RESULTS WITH COMPARE FILE:\n");

    size_t bufferSize = 1000;
    char *buffer = (char *) malloc(bufferSize);

    FILE *file = fopen(compareFileName, "r");

    int charsRead;
    int i = 0;
    int diffs = 0;
    while ((charsRead = getline(&buffer, &bufferSize, file)) != -1) {
        if (i == n) {
            printf("Unequal number of results, other algorithm gives more results than this algorithm.\n");
            break;
        }

        char *result = strtok(buffer, "\n"); //removes the trailing \n
        int resultInt = atoi(result);
        if (scores[i] != resultInt) {
            printf("Scores for sequence with index %d do not match. Calculated score: %d ; other algorithm score: %d\n",
                   i, scores[i], resultInt);
            diffs++;
        }
        i++;
    }
    if (i < n) {
        printf("Unequal number of results, this algorithm gives more results than the other algorithm.\n");
    }

    printf("Comparison ended. Diffs: %d\n", diffs);
    fclose(file);
}

int main(int argc, char **argv) {
    char *graphFile = NULL;
    char *fastqFile = NULL;
    char *outputFile = NULL;
    char *detailedOutputFile = NULL;
    char *compareFile = NULL; //file containing results of other algorithm
    int editDistThreshold = -1;

    int c;
    while ((c = getopt(argc, argv, "g:f:o:c:k:d:")) != -1) {
        switch (c) {
            case 'g':
                graphFile = optarg;
                break;
            case 'f':
                fastqFile = optarg;
                break;
            case 'o':
                outputFile = optarg;
                break;
            case 'c':
                compareFile = optarg;
                break;
            case 'k':
                editDistThreshold = atoi(optarg);
                break;
            case 'd':
                detailedOutputFile = optarg;
                break;
        }
    }

    if (graphFile == NULL) {
        printf("Graph file argument missing. Use -g option to set it.\n");
        return 1;
    }
    if (fastqFile == NULL) {
        printf("Fastq file argument missing. Use -f option to set it.\n");
        return 1;
    }
    if (outputFile == NULL) {
        printf("Output file argument missing. Use -o option to set it.\n");
        return 1;
    }
    if (detailedOutputFile != NULL && editDistThreshold==-1) {
        printf("WARNING: Providing detailed output file argument (-d) is useless without defining edit distance threshold (-k) argument because detailed scores can not be defined without edit distance threshold. \n");
    }
    if (detailedOutputFile == NULL && editDistThreshold!=-1) {
        printf("WARNING: Providing edit distance threshold argument (-k) is useless without defining detailed output file argument (-d). \n");
    }


    struct Graph *graph = loadGraphFromFile(graphFile);

    int sequncesNum;

    SequenceList *sequences = loadSequencesFromFastq(fastqFile, &sequncesNum);

    clock_t startTime = clock();

    SequenceNode *node = sequences->head;
    int *scores = malloc(sizeof(int) * sequences->size);
    struct Score **detailedScores = malloc(sizeof(struct Score*) * sequences->size);
    for (int i = 0; i < sequences->size; i++) {
        navarooAlgorithm(graph, node->sequence);
        detailedScores[i] = getScoresFromGraphAndResetValues(graph,editDistThreshold,&scores[i]);
        printf("Pattern %d/%d - Result: %d\n", (i+1),sequences->size,scores[i]);
        node = node->next;
    }

    clock_t endTime = clock();
    float seconds = (float) (endTime - startTime) / CLOCKS_PER_SEC;
    printf("Algorithm execution took: %f seconds\n", seconds);

    printResults(scores, sequences->size, outputFile);

    if(editDistThreshold!=-1 && detailedOutputFile != NULL) {
        printDetailedScores(detailedScores, sequences->size, detailedOutputFile);
    }

    if (compareFile != NULL) {
        compareResultsToResultsInCompareFile(scores, sequences->size, compareFile);
    } else {
        printf("Skipping results comparison since no compare file has been defined.");
    }

}
